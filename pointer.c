#include<stdio.h>
int swap(int*x,int*y);
int main()
{
    int num1,num2;
    printf("enter the numbers\n");
    scanf("%d,%d",&num1,&num2);
    printf("before swapping num1=%d,num2=%d\n",num1,num2);
    swap(&num1,&num2);
    printf("after swapping num1=%d,num2=%d",num1,num2);
    return 0;
}
    int swap(int*x,int*y)
    {
        int t;
        t=*x;
        *x=*y;
        *y=t;
    }